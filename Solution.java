class Solution {
    
    public static void main(String[] args) {
        System.out.println(new Solution().hammingDistance(1, 4));
    }
    
    private int hammingDistance(int x, int y) {
        //  return Integer.bitCount(x^y);
        int xor = x^y;

        int result = 0;
        while (xor>0){
            if ((xor &1)==1){
                result++;
            }
            xor >>= 1;
        }
        return result;
    }
}